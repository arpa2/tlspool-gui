## \brief     A Qt based GUI for the TLS Pool
## \author    Aschwin Marsman
## \date      2016
## \copyright Copyright 2016-2019, Aschwin Marsman, the ARPA2 project
## \license   SPDX-License-Identifier: GPL-2.0

cmake_minimum_required(VERSION 3.13 FATAL_ERROR)
project(tlspool-gui VERSION 0.0.6 LANGUAGES CXX)


set(CMAKE_BUILD_TYPE Debug)  ## For now hard coded

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include (FeatureSummary)

find_package (ARPA2CM 0.8.0 QUIET NO_MODULE)
set_package_properties (ARPA2CM PROPERTIES
    DESCRIPTION "CMake modules for ARPA2 projects"
    TYPE REQUIRED
    URL "https://gitlab.com/arpa2/arpa2cm/"
    PURPOSE "Required for the CMake build system for ${PROJECT}"
)
if (ARPA2CM_FOUND)
    set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ARPA2CM_MODULE_PATH})
else()
    feature_summary (WHAT ALL)
    message (FATAL_ERROR "ARPA2CM is required.")
endif()

## \todo check should be for g++ or clang.
if (APPLE)
	set(COMPILE_FLAGS "-Weverything -Wno-padded -Wno-c++98-compat")
else()
        set(COMPILE_FLAGS "-Wall -Wextra -Wshadow -Wold-style-cast -Wfloat-equal -Woverloaded-virtual -Wundef -DDEBUG")
endif ()
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${COMPILE_FLAGS}")


## Include (or not) the full compiler output
set(CMAKE_VERBOSE_MAKEFILE ON)

include(MacroGitVersionInfo)
get_project_git_version()

# Find the Qt libraries
find_package(Qt5 5.6 COMPONENTS Core Gui Svg Widgets)
set_package_properties(Qt5 PROPERTIES TYPE REQUIRED PURPOSE "GUI Toolkit")
find_package(TLSPool CONFIG)
set_package_properties(TLSPool PROPERTIES TYPE REQUIRED PURPOSE "TLSPool libraries and daemons")

feature_summary(FATAL_ON_MISSING_REQUIRED_PACKAGES WHAT REQUIRED_PACKAGES_NOT_FOUND)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOMOC_OPTIONS ) # No options
set(CMAKE_AUTORCC ON)

## Executable
add_executable(${PROJECT_NAME}
    src/localidselecetionthread.cpp
    src/main.cpp
    src/pinentrydialog.cpp
    src/pinentrythread.cpp
    src/selectlocalidentitydialog.cpp
    src/systemtrayitem.cpp
    src/tlspoolguisettings.cpp
    src/tlspoolinterface.cpp

    src/resources.qrc
)

# Use the Widgets module from Qt 5.
target_link_libraries(${PROJECT_NAME} Qt5::Widgets Qt5::Svg ARPA2::TLSPool)

install(TARGETS ${PROJECT_NAME}
        RUNTIME DESTINATION bin)

include(CTest)
add_subdirectory(tests)
